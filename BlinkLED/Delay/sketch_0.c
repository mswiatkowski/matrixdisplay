//----- Defines
#define TIMER_DELAY 0x100

//----- Prototype
void GPIO_Toogle( GPIO_TypeDef *PORT, uint32_t pin );
void Delay( uint32_t times );

int main(void)
{
	//Clock enable
	RCC->AHB1ENR = RCC_AHB1ENR_GPIODEN;

	//Pin PD13 out
	GPIOD->MODER &= ( 0x03 << 26 );
	GPIOD->MODER |= ( 0x01 << 26 );

    while(1)
    {
    	GPIO_Toogle( GPIOD, 13 );
    	Delay( TIMER_DELAY );
    }
}

void GPIO_Toogle( GPIO_TypeDef *PORT, uint32_t pin )
{
	if( PORT->IDR & ( 1 << pin ) )
	{
		PORT->ODR &= ~( 1 << pin );
	}
	else
	{
		PORT->ODR |= 1 << pin;
	}
}

void Delay( uint32_t times )
{
	uint32_t i, j;
	for( i = 0; i < times; i++ )
	{
		for( j = 0; j < 1000; j++ )
		{
			;
		}
	}
}