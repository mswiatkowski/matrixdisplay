//----- Defines
#define TIMER_OVERFLOW 0x1000

//----- Prototype
void GPIO_Toogle( GPIO_TypeDef *PORT, uint32_t pin );

int main(void)
{
	//Clock enable
	RCC->AHB1ENR = RCC_AHB1ENR_GPIODEN;
	RCC->APB2ENR = RCC_APB2ENR_TIM1EN;

	//Pin PD13 out
	GPIOD->MODER &= ( 0x03 << 26 );
	GPIOD->MODER |= ( 0x01 << 26 );

	//Timer1 interrupt
	TIM1->PSC = 0x5f;			//Prescaler, wpływa na czas impulsu
	TIM1->CR1 |= 0x01;			//Counter enable
	TIM1->DIER |= 0x01;			//Update inerrupt enable

    while(1)
    {
    	uint16_t counter = TIM1->CNT;
    	if(  counter == TIMER_OVERFLOW ) //16 bitowy
    	{
    		TIM1->CNT = 0;
    		GPIO_Toogle( GPIOD, 13 );
    	}
    }
}

void GPIO_Toogle( GPIO_TypeDef *PORT, uint32_t pin )
{
	if( PORT->IDR & ( 1 << pin ) )
	{
		PORT->ODR &= ~( 1 << pin );
	}
	else
	{
		PORT->ODR |= 1 << pin;
	}
}