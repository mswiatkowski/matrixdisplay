int main(void)
{
	//Clock enable
	RCC->AHB1ENR = RCC_AHB1ENR_GPIODEN;
	RCC->APB2ENR = RCC_APB2ENR_TIM1EN;

	//Pin PD13 out
	GPIOD->MODER &= ( 0x03 << 26 );
	GPIOD->MODER |= ( 0x01 << 26 );

	//Timer1 interrupt
	TIM1->PSC = 0x5f;			//Prescaler, wp�ywa na czas impulsu
	TIM1->CR1 |= 0x01;			//Counter enable
	TIM1->DIER |= 0x01;			//Update inerrupt enable

    while(1)
    {
    	if( TIM1->SR & 0x01 )
    	{
    		TIM1->SR &= ~0x01;
    		if( GPIOD->IDR & ( 1 << 13 ) )
    		{
    			GPIOD->ODR &= ~( 1 << 13 );
    		}
    		else
    		{
    			GPIOD->ODR |= 1 << 13;
    		}
    	}
    }
}