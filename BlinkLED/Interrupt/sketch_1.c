//----- Defines
#define	TIMER_START_VALUE 0x5000
//----- Prototype
void GPIO_Toogle( GPIO_TypeDef *PORT, uint32_t pin );
//test
//----- Global variables
uint32_t toogleSpeed = TIMER_START_VALUE;

int main(void)
{
	//Clock enable
	RCC->AHB1ENR = RCC_AHB1ENR_GPIODEN;

	//Pin PD13 out
	GPIOD->MODER &= ( 0x03 << 26 );
	GPIOD->MODER |= ( 0x01 << 26 );
	RCC->APB2ENR = RCC_APB2ENR_TIM1EN;

	//Timer1 interrupt
	TIM1->PSC = 0x5f;			//Prescaler, wpływa na czas impulsu
	TIM1->CR1 |= 0x01;			//Counter enable
	TIM1->DIER |= 0x01;			//Update inerrupt enable

	NVIC_EnableIRQ( TIM1_UP_TIM10_IRQn );

    while(1)
    {
    }
}

void TIM1_UP_TIM10_IRQHandler (void)
{
	TIM1->SR &= ~0x01;			//Zerujemy flagę przerwania
	TIM1->ARR = toogleSpeed;			//Szybkosc przerwania regulujemy
	toogleSpeed += 0x100;
	GPIO_Toogle( GPIOD, 13 );
}

void GPIO_Toogle( GPIO_TypeDef *PORT, uint32_t pin )
{
	if( PORT->IDR & ( 1 << pin ) )
	{
		PORT->ODR &= ~( 1 << pin );
	}
	else
	{
		PORT->ODR |= 1 << pin;
	}
}