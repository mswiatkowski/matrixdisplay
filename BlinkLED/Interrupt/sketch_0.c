/*Nag��wki musisz doda� takie jak mielismy w pierwszym ;) 

Mo�liwe, �e minimalnie r�ni� sie rejestry timera, trzeba 
sprawdzi� w nocie

Np. rejestry clock�w tez si� troche r�ni� nazw�)*/

int main(void)
{
	//Clock enable
	RCC->AHB1ENR = RCC_AHB1ENR_GPIODEN;
	RCC->APB2ENR = RCC_APB2ENR_TIM1EN;

	//Pin PD13 out
	GPIOD->MODER &= ( 0x03 << 26 );
	GPIOD->MODER |= ( 0x01 << 26 );

	//Timer1 interrupt
	TIM1->PSC = 0x5f;			//Prescaler, wp�ywa na czas impulsu
	TIM1->CR1 |= 0x01;			//Counter enable
	TIM1->DIER |= 0x01;			//Update inerrupt enable

	NVIC_EnableIRQ( TIM1_UP_TIM10_IRQn );

    while(1)
    {
    }
}

void TIM1_UP_TIM10_IRQHandler (void)
{
	TIM1->SR &= ~0x01;			//Zerujemy flag� przerwania
	if( GPIOD->IDR & ( 1 << 13 ) )
	{
		GPIOD->ODR &= ~( 1 << 13 );
	}
	else
	{
		GPIOD->ODR |= 1 << 13;
	}
}